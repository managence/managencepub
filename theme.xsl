<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:css="http://namespaces.plone.org/diazo/css" xmlns:dv="http://namespaces.plone.org/diazo" xmlns:dyn="http://exslt.org/dynamic" xmlns:esi="http://www.edge-delivery.org/esi/1.0" xmlns:exsl="http://exslt.org/common" xmlns:str="http://exslt.org/strings" xmlns:xhtml="http://www.w3.org/1999/xhtml" version="1.0" exclude-result-prefixes="exsl str css dv dyn xhtml esi">

<xsl:param name="path"/>


    <xsl:variable name="normalized_path"><xsl:value-of select="$path"/><xsl:if test="substring($path, string-length($path)) != '/'">/</xsl:if></xsl:variable>

    <xsl:output method="xml" indent="no" omit-xml-declaration="yes" media-type="text/html" encoding="UTF-8" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="text()">
        <!-- Filter out quoted &#13; -->
        <xsl:value-of select="str:replace(., '&#13;&#10;', '&#10;')"/>
    </xsl:template>

    <xsl:template match="style/text()|script/text()">
        <xsl:value-of select="str:replace(., '&#13;&#10;', '&#10;')" disable-output-escaping="yes"/>
    </xsl:template>

    <xsl:template match="/html/@xmlns">
        <!-- Filter out -->
    </xsl:template>


    <xsl:template match="/"><xsl:choose>
<xsl:when test="starts-with($normalized_path, '/raw/')"><xsl:apply-templates select="@*|node()"/></xsl:when>

<xsl:when test="(//body[@class and contains(concat(' ', normalize-space(@class), ' '), ' rawpage ')])"><xsl:apply-templates select="@*|node()"/></xsl:when>

<xsl:when test="starts-with($normalized_path, '/fr/')"><xsl:apply-templates select="." mode="r1"/></xsl:when>

<xsl:otherwise><xsl:apply-templates select="." mode="r2"/></xsl:otherwise>
</xsl:choose></xsl:template>

    <!--THEME r1: html/fr/theme.html-->
    <xsl:template match="/" mode="r1"><html><xsl:text>
</xsl:text><head><xsl:text>
</xsl:text><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><xsl:text>
</xsl:text><xsl:apply-templates select="/html/head/title"/><xsl:text>
  </xsl:text><link rel="stylesheet" href="/css/bootstrap.css"/><xsl:text>
  </xsl:text><link rel="stylesheet" href="/css/bootswatch.min.css"/><xsl:text>
  </xsl:text><link rel="stylesheet" href="/css/managence-bootstrap.css"/><xsl:text>
  </xsl:text><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/><xsl:text>
  </xsl:text><script src="http://code.jquery.com/jquery-2.1.0.min.js"/><xsl:text>
   </xsl:text><script language="JavaScript1.2"><xsl:variable name="tag_text">
  &lt;!--
    //  PopUp --
    //  Institute for Human and Machine Cognition, http://www.ihmc.us/
    //  CmapTools, Version 4.11, http://cmap.ihmc.us/
    //  Modify by: CmapTools Developers, cmapsupport@ihmc.us
    //  Date: 12/14/2006

    var IE = 0;
    var IE4PC = 0;
    var NS = 0;
    var GECKO = 0;

    var openpopups = new Array();

    if (document.all) {                 // Internet Explorer Detected
	   IE = true;	
    }
    else if (document.layers) {         // Netscape Navigator Detected
	   NS = true;	
    }
    else if (document.getElementById) { // Netscape 6 Detected
       GECKO = true;
    }
    else {
	   alert("Unrecognized Browser Detected::\nSorry, your browser is not compatible.");
    }

    if (IE)
    {
	   OS = navigator.platform;
	   VER = new String(navigator.appVersion);
	   VER = VER.substr (0, VER.indexOf(" "));
	   if ((VER &lt; 4.8) &amp;&amp; (OS == "Win32")) 
	   {
	       IE4PC = 1;
	   }
    }

    function handleResize() {
	    location.reload();
	    return false;
    }


    if ((NS) &amp;&amp; (navigator.platform == "MacPPC")) {
	   window.captureEvents (Event.RESIZE);
	   window.onresize = handleResize;
    }

    function openResource(event, resourcePath, resourceName, resourceType)
    {
	   var newwindow;

       // Resource MIMETypes are enumerated as follows: 
       // cMap     = Undefined
       // Image    = 0
       // Video    = 1
       // Text     = 2
       // Audio    = 3

       popDown();

	   switch (resourceType) 
	   {
	    case 0:     //Image
                    newwindow = window.open (resourcePath,resourceName);
                    newwindow.onBlur = newwindow.focus()
                    var text ='&lt;center&gt;&lt;img src=\"';
                    text += resourcePath;
                    text +='\"&gt; &lt;/center&gt;';
             break;
		
	    case 1:  text   // Video
		     newwindow = window.open (resourcePath,resourceName);
                     newwindow.onBlur = newwindow.focus()
                     var text ='&lt;embed src=\"';
                     text += resourcePath;
                     text +='\" autostart=true&gt; &lt;/embed&gt; ';
             break;

	    case 2:      // Text or Unknown 
		     newwindow = window.open(resourcePath,resourceName);
                     newwindow.onBlur = newwindow.focus()
              break;

	    case 3:       // Audio
                    newwindow = window.open (resourcePath,resourceName);
                    newwindow.onBlur = newwindow.focus()
                    var text ='&lt;embed src=\"';
                    text += resourcePath;
                    text +='\" autoplay=true&gt; &lt;/embed&gt; ';
             break;

	    case 4:
                    newwindow = window.open(resourcePath,resourceName);
                    newwindow.onBlur = newwindow.focus()
	     break;
	    }
        return;
    }



    function popUpEvt(event, popupName)
    {
	popDown();

	if (GECKO)
	{
	    document.getElementById(popupName).style.left = event.layerX;
	    document.getElementById(popupName).style.top = event.layerY;
	    document.getElementById(popupName).style.background = "#B3B3B3";
	    document.getElementById(popupName).style.visibility = "visible";
	    openpopups.push(popupName);
	}
	else if (NS) 
	{
	    document.layers[popupName].moveTo (event.pageX, event.pageY);
	    document.layers[popupName].bgColor = "#B3B3B3";
	    document.layers[popupName].visibility = "show";
	    openpopups.push(popupName);
	}
	else // if (IE)
	{
	    window.event.cancelBubble = true;
	    if (!IE4PC) {
		document.all[popupName].style.backgroundColor = "#B3B3B3";
	    }	
	    document.all[popupName].style.left = window.event.clientX + document.body.scrollLeft;
	    document.all[popupName].style.top = window.event.clientY + document.body.scrollTop;
	    document.all[popupName].style.visibility = "visible";
	    openpopups[openpopups.length] = popupName;
	}
	return false;
    }

    function popDown()
    {
	var popupname;
	
	for (var i = 0; i &lt; openpopups.length; i++) 
	{
	    popupname = new String (openpopups[i]);      
	    if (GECKO) {
		document.getElementById(popupname).style.visibility = "hidden";
	    }
	    else if (NS) {
		document.layers[popupname].visibility = "hide";
	    }
	    else {
		document.all[popupname].style.visibility = "hidden";
	    }
	}
	openpopups = new Array();
	return;
    }

    function popDownNoGecko() 
    {
	var popupname;
	for (var i = 0; i &lt; openpopups.length; i++) 
	{
	    popupname = new String (openpopups[i]);      
	    if (GECKO) {
           //  document.getElementById(popupname).style.visibility = "hidden"; 
	        return;   // erased for test purposes
	    }  
	    else if (NS) {
	        document.layers[popupname].visibility = "hide";
	    }
	    else {
	        document.all[popupname].style.visibility = "hidden";
	    }
	}
	openpopups = new Array();
        return;
    }
  //--&gt;
  </xsl:variable><xsl:value-of select="$tag_text" disable-output-escaping="yes"/></script><xsl:text>
</xsl:text></head><xsl:text>
</xsl:text><body lang="fr-FR">
<div class="container"><xsl:text>
</xsl:text><div class="row"><xsl:text> 
</xsl:text><div class="col-md-2 text-center"><xsl:text>
  </xsl:text><p><a href="http://www.managence.com"><img src="/images/images/logo_managence.png" alt="Managence (tm) par Christopher Mann" name="logo" id="logo" longdesc="http://www.managence.com" width="100%"/></a></p><xsl:text>
  </xsl:text><div class="navbar navbar-default"><xsl:text>
  </xsl:text><div class="navbar-header"><xsl:text>
    </xsl:text><button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse"><xsl:text>
      </xsl:text><span class="icon-bar"/><xsl:text>
      </xsl:text><span class="icon-bar"/><xsl:text>
      </xsl:text><span class="icon-bar"/><xsl:text>
    </xsl:text></button><xsl:text>
    </xsl:text><a class="navbar-brand" href="/fr/">Managence.com</a><xsl:text>
  </xsl:text></div><xsl:text>
</xsl:text></div><xsl:text>
</xsl:text><div class="btn-group btn-group-justified"><xsl:text>
  </xsl:text><a href="#" class="btn btn-default disabled">CN</a><xsl:text>
  </xsl:text><a href="/en/" class="btn btn-default">EN</a><xsl:text>
  </xsl:text><a href="#" class="btn btn-default active">FR</a><xsl:text>
</xsl:text></div><xsl:text>
</xsl:text><div id="managence-menu"><xsl:choose>
<xsl:when test="(//div[@id='managence-menu'])"><xsl:apply-templates select="//div[@id='managence-menu']/node()"/></xsl:when>
<xsl:otherwise>
  <div class="bs-component"><xsl:text>
    </xsl:text><ul class="nav nav-pills nav-stacked"><xsl:text>
      </xsl:text><li><a href="/fr/">Home</a></li><xsl:text>
      </xsl:text><li><xsl:text>
        </xsl:text><a href="/fr/services/"><xsl:text>
          Services </xsl:text><span class="caret"/><xsl:text>
        </xsl:text></a><xsl:text>
      </xsl:text><ul class="text-left"><xsl:text>
          </xsl:text><li><a href="/fr/services/team-consulting/">Organisation d'équipe(s)</a></li><xsl:text>
          </xsl:text><li><a href="/fr/services/pmo/">Processus  &amp; Méthode</a></li><xsl:text>
          </xsl:text><li><a href="/fr/services/project-management/">Gestion de projet opérationelle</a></li><xsl:text>
          </xsl:text></ul><xsl:text>
      </xsl:text></li><xsl:text>
      </xsl:text><li><a href="/fr/bio/">Bio</a></li><xsl:text>
      </xsl:text><li><a href="http://fr.wordpress.managence.com/">Blog</a></li><xsl:text>
      </xsl:text><li><a href="https://www.managence.com/cmap/rid=1N685WM1W-PXTK9H-GW">Concept Maps</a></li><xsl:text>
      </xsl:text><li><a href="/fr/contact/">Contact</a></li><xsl:text>
    </xsl:text></ul><xsl:text>
  </xsl:text></div>
  </xsl:otherwise>
</xsl:choose></div><xsl:text>
  </xsl:text><p><xsl:text>
  </xsl:text><a name="contact">+33 781 811 811</a><br/><span style="font-size: x-small"><a href="mailto:christopher@mann.fr">christopher@managence.com</a></span></p><xsl:text>
  </xsl:text><div style="background-color: #99cd9a;"><xsl:text>
   </xsl:text><br/><xsl:text> </xsl:text><br/><xsl:text>
  </xsl:text></div><xsl:text>
</xsl:text></div><xsl:text>
</xsl:text><div class="col-md-8"><xsl:text>
</xsl:text><xsl:choose><xsl:when test="(not(//div[@id='managence-breadcrumb']))"/><xsl:otherwise><div id="managence-breadcrumb"><xsl:choose>
<xsl:when test="(//div[@id='managence-breadcrumb'])"><xsl:apply-templates select="//div[@id='managence-breadcrumb']/node()"/></xsl:when>
<xsl:otherwise>
<ul class="breadcrumb"><xsl:text>
  </xsl:text><li><a href="/en/">Home</a></li><xsl:text>
  </xsl:text><li><a href="/en/services">Services</a></li><xsl:text>
  </xsl:text><li class="active">PMO</li><xsl:text>
</xsl:text></ul>
</xsl:otherwise>
</xsl:choose></div></xsl:otherwise></xsl:choose><xsl:text>
</xsl:text><div id="managence-content"><xsl:choose>
<xsl:when test="(//div[@id='managence-content'])"><xsl:apply-templates select="//div[@id='managence-content']/node()"/></xsl:when>
<xsl:when test="(//div[@id='wrapper'])"><xsl:apply-templates select="//div[@id='wrapper']/node()"/></xsl:when>
<xsl:when test="(//main[@id='main'])"><xsl:apply-templates select="//main[@id='main']/node()"/></xsl:when>
<xsl:when test="not(//main[@id='managence-content'])"><xsl:apply-templates select="/html/body/node()"/></xsl:when>
<xsl:otherwise>
<h1>Software Project Services</h1>
<p>Managence (tm) by Christopher Mann can bring you value in:<br/><xsl:text>
</xsl:text></p><ul><xsl:text>
  </xsl:text><li>Specification</li><xsl:text>
  </xsl:text><li>Development</li><xsl:text>
  </xsl:text><li>Program  and Project Management Office</li><xsl:text>
  </xsl:text><li>Methods  and Methodologies</li><xsl:text>
  </xsl:text><li>IT  Gouvernance</li><xsl:text>
  </xsl:text><li>Legal  and Compliance</li><xsl:text>
  </xsl:text><li>Management</li><xsl:text>
  </xsl:text><li>Business  Consulting</li><xsl:text>
  </xsl:text><li>Original  Software Creation</li><xsl:text>
</xsl:text></ul>
<p>by  innovating for businesses drawing on extensive experience in internet  projects applying invention with vision and great respect for  collaboration.</p>
<h2><a name="services">Services</a></h2>
<p>Managence (tm) by Christopher Mann brings IT project and organizational services  calibrated to the needs of the client and end-users.</p>
<p>Key  strengths in rigorous conceptualization and state-of-the-art  approaches allow per-diem consulting to bring you long-lasting value.</p>
<h3><a name="spm">Processus  &amp; Method</a></h3>
<p>Routines  and objective measures create channels for reliable information flow.  Managence (tm) by Christopher Mann reinforces rigorous analysis,  common sense, and method application in multiple aspects of everyday  work.</p>
<p>An  example of this process management expertise can be seen from work  with the French National Employment Agency. Actively maintained  project work breakdown structures with management team capacity  allocation brought both management control and co-variant portfolio  influences in a deterministic capacity-planning solution.</p>
<p><em>I  hired Christopher as a PMO for several missions. His kindness  equalshis professionalism and his creativity. I do recommend him in  any situationwhere thinking out of the box is required!</em></p>
<p><em>-  Fadi Gemayel, CEO of Daylight Group</em></p>
<h3><a name="sto">Team  Organisation</a></h3>
<p>Collaborative  teams, internal and external, are reinforced by mutual mastery of  respective deliverables. Managence by Christopher Mann brings value  by first understanding and then facilitating the matrices of inputs,  outputs, and needs of different actors.</p>
<p>An  example of this work is in the DSI of Wonderbox, serving as acting  assistant CIO during the transition of a purchase by  Pinot-Printemps-Redoute.</p>
<p><em>"Environnement  international. Il y avait une équipe de 30 développeurs,  Christopher était le lien entre les équipes fonctionnel et  techniques. Très très bon sur la partie AGILE."</em></p>
<p><em>-  Fatih Gezen,<br/>
Directeur  chez ANIL IS</em></p>
<h3><a name="sopm">Operational Project  Management</a></h3>
<p>Operational  project management materializes business needs with increased added  value and customer satisfaction. Do you have a project, IT or  otherwise, that you would like to get off the ground? Managence by  Christopher Mann project management works.</p>
<p>An  example of this work is in the mastery of a full operational project  cycle in software for town ordinances : from value-added analysis to  Agile and extreme programming to rollout. The result was three  distinct open-source projects, one of which is in active use  throughout the world.</p>
<p><em>"Christopher  was a great advocate and produced world class results in his  position. I continue to be impressed with the innovation and  execution of his efforts and recommend him highly."</em></p>
<p><em>Roy  Rubin,<br/>
  Founder  of Magento e-Commerce</em></p>
<h2><a name="bio">Bio</a></h2>
<p>Christopher  Mann,</p>
  <p>educated  in economics and computer-science from</p>
<p>Oberlin  College, Sciences Po., Supélec,</p>
  <p>brings  experience from</p>
<p>Startups:  Magento, MBA Multimédia, Wonderbox, ...<br/>
  Creations:  E-Delib, XSpyDer, PyrAxe, ...<br/>
  Opensource:  Frameworks, Communautés, ...<br/>
  Business  Consulting: Zend, Pro Info, ...<br/>
  ITIL:  Bureaux Veritas, Alstom, ...<br/>
  PMO:  ANPE DSI, BNP Paribas Fortis, ...</p>
</xsl:otherwise>
</xsl:choose></div><xsl:text>
</xsl:text></div><xsl:text>
</xsl:text></div><xsl:text>
</xsl:text></div>
<xsl:apply-templates select="/div"/>
</body><xsl:text>
</xsl:text></html>
</xsl:template>

    <!--THEME r2: html/en/theme.html-->
    <xsl:template match="/" mode="r2"><html><xsl:text>
</xsl:text><head><xsl:text>
</xsl:text><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><xsl:text>
</xsl:text><xsl:apply-templates select="/html/head/title"/><xsl:text>
  </xsl:text><link rel="stylesheet" href="/css/bootstrap.css"/><xsl:text>
  </xsl:text><link rel="stylesheet" href="/css/bootswatch.min.css"/><xsl:text>
  </xsl:text><link rel="stylesheet" href="/css/managence-bootstrap.css"/><xsl:text>
  </xsl:text><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/><xsl:text>
  </xsl:text><script src="http://code.jquery.com/jquery-2.1.0.min.js"/><xsl:text>
   </xsl:text><script language="JavaScript1.2"><xsl:variable name="tag_text">
  &lt;!--
    //  PopUp --
    //  Institute for Human and Machine Cognition, http://www.ihmc.us/
    //  CmapTools, Version 4.11, http://cmap.ihmc.us/
    //  Modify by: CmapTools Developers, cmapsupport@ihmc.us
    //  Date: 12/14/2006

    var IE = 0;
    var IE4PC = 0;
    var NS = 0;
    var GECKO = 0;

    var openpopups = new Array();

    if (document.all) {                 // Internet Explorer Detected
	   IE = true;	
    }
    else if (document.layers) {         // Netscape Navigator Detected
	   NS = true;	
    }
    else if (document.getElementById) { // Netscape 6 Detected
       GECKO = true;
    }
    else {
	   alert("Unrecognized Browser Detected::\nSorry, your browser is not compatible.");
    }

    if (IE)
    {
	   OS = navigator.platform;
	   VER = new String(navigator.appVersion);
	   VER = VER.substr (0, VER.indexOf(" "));
	   if ((VER &lt; 4.8) &amp;&amp; (OS == "Win32")) 
	   {
	       IE4PC = 1;
	   }
    }

    function handleResize() {
	    location.reload();
	    return false;
    }


    if ((NS) &amp;&amp; (navigator.platform == "MacPPC")) {
	   window.captureEvents (Event.RESIZE);
	   window.onresize = handleResize;
    }

    function openResource(event, resourcePath, resourceName, resourceType)
    {
	   var newwindow;

       // Resource MIMETypes are enumerated as follows: 
       // cMap     = Undefined
       // Image    = 0
       // Video    = 1
       // Text     = 2
       // Audio    = 3

       popDown();

	   switch (resourceType) 
	   {
	    case 0:     //Image
                    newwindow = window.open (resourcePath,resourceName);
                    newwindow.onBlur = newwindow.focus()
                    var text ='&lt;center&gt;&lt;img src=\"';
                    text += resourcePath;
                    text +='\"&gt; &lt;/center&gt;';
             break;
		
	    case 1:  text   // Video
		     newwindow = window.open (resourcePath,resourceName);
                     newwindow.onBlur = newwindow.focus()
                     var text ='&lt;embed src=\"';
                     text += resourcePath;
                     text +='\" autostart=true&gt; &lt;/embed&gt; ';
             break;

	    case 2:      // Text or Unknown 
		     newwindow = window.open(resourcePath,resourceName);
                     newwindow.onBlur = newwindow.focus()
              break;

	    case 3:       // Audio
                    newwindow = window.open (resourcePath,resourceName);
                    newwindow.onBlur = newwindow.focus()
                    var text ='&lt;embed src=\"';
                    text += resourcePath;
                    text +='\" autoplay=true&gt; &lt;/embed&gt; ';
             break;

	    case 4:
                    newwindow = window.open(resourcePath,resourceName);
                    newwindow.onBlur = newwindow.focus()
	     break;
	    }
        return;
    }



    function popUpEvt(event, popupName)
    {
	popDown();

	if (GECKO)
	{
	    document.getElementById(popupName).style.left = event.layerX;
	    document.getElementById(popupName).style.top = event.layerY;
	    document.getElementById(popupName).style.background = "#B3B3B3";
	    document.getElementById(popupName).style.visibility = "visible";
	    openpopups.push(popupName);
	}
	else if (NS) 
	{
	    document.layers[popupName].moveTo (event.pageX, event.pageY);
	    document.layers[popupName].bgColor = "#B3B3B3";
	    document.layers[popupName].visibility = "show";
	    openpopups.push(popupName);
	}
	else // if (IE)
	{
	    window.event.cancelBubble = true;
	    if (!IE4PC) {
		document.all[popupName].style.backgroundColor = "#B3B3B3";
	    }	
	    document.all[popupName].style.left = window.event.clientX + document.body.scrollLeft;
	    document.all[popupName].style.top = window.event.clientY + document.body.scrollTop;
	    document.all[popupName].style.visibility = "visible";
	    openpopups[openpopups.length] = popupName;
	}
	return false;
    }

    function popDown()
    {
	var popupname;
	
	for (var i = 0; i &lt; openpopups.length; i++) 
	{
	    popupname = new String (openpopups[i]);      
	    if (GECKO) {
		document.getElementById(popupname).style.visibility = "hidden";
	    }
	    else if (NS) {
		document.layers[popupname].visibility = "hide";
	    }
	    else {
		document.all[popupname].style.visibility = "hidden";
	    }
	}
	openpopups = new Array();
	return;
    }

    function popDownNoGecko() 
    {
	var popupname;
	for (var i = 0; i &lt; openpopups.length; i++) 
	{
	    popupname = new String (openpopups[i]);      
	    if (GECKO) {
           //  document.getElementById(popupname).style.visibility = "hidden"; 
	        return;   // erased for test purposes
	    }  
	    else if (NS) {
	        document.layers[popupname].visibility = "hide";
	    }
	    else {
	        document.all[popupname].style.visibility = "hidden";
	    }
	}
	openpopups = new Array();
        return;
    }
  //--&gt;
  </xsl:variable><xsl:value-of select="$tag_text" disable-output-escaping="yes"/></script><xsl:text>
</xsl:text></head><xsl:text>
</xsl:text><body lang="en-US">
<div class="container"><xsl:text>
</xsl:text><div class="row"><xsl:text> 
</xsl:text><div class="col-md-2 text-center"><xsl:text>
  </xsl:text><p><a href="http://www.managence.com"><img src="/images/images/logo_managence.png" alt="Managence (tm) by Christopher Mann" name="logo" id="logo" longdesc="http://www.managence.com" width="100%"/></a></p><xsl:text>
  </xsl:text><div class="navbar navbar-default"><xsl:text>
  </xsl:text><div class="navbar-header"><xsl:text>
    </xsl:text><button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse"><xsl:text>
      </xsl:text><span class="icon-bar"/><xsl:text>
      </xsl:text><span class="icon-bar"/><xsl:text>
      </xsl:text><span class="icon-bar"/><xsl:text>
    </xsl:text></button><xsl:text>
    </xsl:text><a class="navbar-brand" href="/fr/">Managence.com</a><xsl:text>
  </xsl:text></div><xsl:text>
</xsl:text></div><xsl:text>
</xsl:text><div class="btn-group btn-group-justified"><xsl:text>
  </xsl:text><a href="#" class="btn btn-default disabled" id="man-lang-cn">CN</a><xsl:text>
  </xsl:text><a href="#" class="btn btn-default active">EN</a><xsl:text>
  </xsl:text><a href="/fr/" class="btn btn-default">FR</a><xsl:text>
</xsl:text></div><xsl:text>
  </xsl:text><div id="managence-menu"><xsl:choose>
<xsl:when test="(//div[@id='managence-menu'])"><xsl:apply-templates select="//div[@id='managence-menu']/node()"/></xsl:when>
<xsl:otherwise>
  <div class="bs-component"><xsl:text>
    </xsl:text><ul class="nav nav-pills nav-stacked"><xsl:text>
      </xsl:text><li><a href="/en/">Home</a></li><xsl:text>
      </xsl:text><li><xsl:text>
        </xsl:text><a href="/en/services/"><xsl:text>
          Services </xsl:text><span class="caret"/><xsl:text>
        </xsl:text></a><xsl:text>
        </xsl:text><ul class="text-left"><xsl:text>
          </xsl:text><li><a href="/en/services/team-consulting/">Team  Organisation</a></li><xsl:text>
          </xsl:text><li><a href="/en/services/pmo/">Processus  &amp; Method</a></li><xsl:text>
          </xsl:text><li><a href="/en/services/project-management/">Operational Project  Management</a></li><xsl:text>
        </xsl:text></ul><xsl:text>
      </xsl:text></li><xsl:text>
      </xsl:text><li><a href="/en/bio/">Bio</a></li><xsl:text>
      </xsl:text><li><a href="http://en.wordpress.managence.com/">Blog</a></li><xsl:text>
      </xsl:text><li><a href="https://www.managence.com/cmap/rid=1N685WM1W-PXTK9H-GW">Concept Maps</a></li><xsl:text>
      </xsl:text><li><a href="/en/contact/">Contact</a></li><xsl:text>
    </xsl:text></ul><xsl:text>
    </xsl:text></div>
  </xsl:otherwise>
</xsl:choose></div><xsl:text>
    </xsl:text><p><xsl:text>
  </xsl:text><a name="contact">+33 781 811 811</a><br/><span style="font-size: x-small"><a href="mailto:christopher@mann.fr">christopher@managence.com</a></span></p><xsl:text>
    </xsl:text><div style="background-color: #99cd9a;"><xsl:text>
    </xsl:text><br/><xsl:text> </xsl:text><br/><xsl:text> 
 </xsl:text></div><xsl:text> 
</xsl:text></div><xsl:text>
</xsl:text><div class="col-md-8"><xsl:text>
</xsl:text><xsl:choose><xsl:when test="(not(//div[@id='managence-breadcrumb']))"/><xsl:otherwise><div id="managence-breadcrumb"><xsl:choose>
<xsl:when test="(//div[@id='managence-breadcrumb'])"><xsl:apply-templates select="//div[@id='managence-breadcrumb']/node()"/></xsl:when>
<xsl:otherwise>
<ul class="breadcrumb"><xsl:text>
  </xsl:text><li><a href="/en/">Home</a></li><xsl:text>
  </xsl:text><li><a href="/en/services">Services</a></li><xsl:text>
  </xsl:text><li class="active">PMO</li><xsl:text>
</xsl:text></ul>
</xsl:otherwise>
</xsl:choose></div></xsl:otherwise></xsl:choose><xsl:text>
</xsl:text><div id="managence-content"><xsl:choose>
<xsl:when test="(//div[@id='managence-content'])"><xsl:apply-templates select="//div[@id='managence-content']/node()"/></xsl:when>
<xsl:when test="(//div[@id='wrapper'])"><xsl:apply-templates select="//div[@id='wrapper']/node()"/></xsl:when>
<xsl:when test="(//main[@id='main'])"><xsl:apply-templates select="//main[@id='main']/node()"/></xsl:when>
<xsl:when test="not(//main[@id='managence-content'])"><xsl:apply-templates select="/html/body/node()"/></xsl:when>
<xsl:otherwise>
<h1>Software Project Services</h1>
<p>Managence (tm) by Christopher Mann can bring you value in:<br/><xsl:text>
</xsl:text></p><ul><xsl:text>
  </xsl:text><li>Specification</li><xsl:text>
  </xsl:text><li>Development</li><xsl:text>
  </xsl:text><li>Program  and Project Management Office</li><xsl:text>
  </xsl:text><li>Methods  and Methodologies</li><xsl:text>
  </xsl:text><li>IT  Gouvernance</li><xsl:text>
  </xsl:text><li>Legal  and Compliance</li><xsl:text>
  </xsl:text><li>Management</li><xsl:text>
  </xsl:text><li>Business  Consulting</li><xsl:text>
  </xsl:text><li>Original  Software Creation</li><xsl:text>
</xsl:text></ul>
<p>by  innovating for businesses drawing on extensive experience in internet  projects applying invention with vision and great respect for  collaboration.</p>
<h2><a name="services">Services</a></h2>
<p>Managence (tm) by Christopher Mann brings IT project and organizational services  calibrated to the needs of the client and end-users.</p>
<p>Key  strengths in rigorous conceptualization and state-of-the-art  approaches allow per-diem consulting to bring you long-lasting value.</p>
<h3><a name="spm">Processus  &amp; Method</a></h3>
<p>Routines  and objective measures create channels for reliable information flow.  Managence (tm) by Christopher Mann reinforces rigorous analysis,  common sense, and method application in multiple aspects of everyday  work.</p>
<p>An  example of this process management expertise can be seen from work  with the French National Employment Agency. Actively maintained  project work breakdown structures with management team capacity  allocation brought both management control and co-variant portfolio  influences in a deterministic capacity-planning solution.</p>
<p><em>I  hired Christopher as a PMO for several missions. His kindness  equalshis professionalism and his creativity. I do recommend him in  any situationwhere thinking out of the box is required!</em></p>
<p><em>-  Fadi Gemayel, CEO of Daylight Group</em></p>
<h3><a name="sto">Team  Organisation</a></h3>
<p>Collaborative  teams, internal and external, are reinforced by mutual mastery of  respective deliverables. Managence by Christopher Mann brings value  by first understanding and then facilitating the matrices of inputs,  outputs, and needs of different actors.</p>
<p>An  example of this work is in the DSI of Wonderbox, serving as acting  assistant CIO during the transition of a purchase by  Pinot-Printemps-Redoute.</p>
<p><em>"Environnement  international. Il y avait une équipe de 30 développeurs,  Christopher était le lien entre les équipes fonctionnel et  techniques. Très très bon sur la partie AGILE."</em></p>
<p><em>-  Fatih Gezen,<br/>
Directeur  chez ANIL IS</em></p>
<h3><a name="sopm">Operational Project  Management</a></h3>
<p>Operational  project management materializes business needs with increased added  value and customer satisfaction. Do you have a project, IT or  otherwise, that you would like to get off the ground? Managence by  Christopher Mann project management works.</p>
<p>An  example of this work is in the mastery of a full operational project  cycle in software for town ordinances : from value-added analysis to  Agile and extreme programming to rollout. The result was three  distinct open-source projects, one of which is in active use  throughout the world.</p>
<p><em>"Christopher  was a great advocate and produced world class results in his  position. I continue to be impressed with the innovation and  execution of his efforts and recommend him highly."</em></p>
<p><em>Roy  Rubin,<br/>
  Founder  of Magento e-Commerce</em></p>
<h2><a name="bio">Bio</a></h2>
<p>Christopher  Mann,</p>
  <p>educated  in economics and computer-science from</p>
<p>Oberlin  College, Sciences Po., Supélec,</p>
  <p>brings  experience from</p>
<p>Startups:  Magento, MBA Multimédia, Wonderbox, ...<br/>
  Creations:  E-Delib, XSpyDer, PyrAxe, ...<br/>
  Opensource:  Frameworks, Communautés, ...<br/>
  Business  Consulting: Zend, Pro Info, ...<br/>
  ITIL:  Bureaux Veritas, Alstom, ...<br/>
  PMO:  ANPE DSI, BNP Paribas Fortis, ...</p>
</xsl:otherwise>
</xsl:choose></div><xsl:text>
</xsl:text></div><xsl:text>
</xsl:text></div><xsl:text>
</xsl:text></div>
<xsl:apply-templates select="/div"/>
</body><xsl:text>
</xsl:text></html>
</xsl:template>

    <xsl:template mode="raw" match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates mode="raw" select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template mode="raw" match="text()">
        <!-- Filter out quoted &#13; -->
        <xsl:value-of select="str:replace(., '&#13;&#10;', '&#10;')"/>
    </xsl:template>
    <xsl:template mode="raw" match="style/text()|script/text()">
        <xsl:value-of select="str:replace(., '&#13;&#10;', '&#10;')" disable-output-escaping="yes"/>
    </xsl:template>
    <xsl:template mode="raw" match="/html/@xmlns">
        <!-- Filter out -->
    </xsl:template>
</xsl:stylesheet>
