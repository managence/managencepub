import markdown
import os, sys

#import codecs

# import html.entities

def convert (mdfilefullpath):
	print mdfilefullpath
	#outfileContent = open(mdfilefullpath[0:-2] + "content", mode="w")
	#outfile = codecs.open(mdfilefullpath[0:-2] + "html", mode="w", encoding="utf-8", errors='ignore')
	outfile = open(mdfilefullpath[0:-2] + "html", mode="w")

	# From: http://www3.telus.net/danpeirce/notes/python_markdown.html
	mkin = open(mdfilefullpath)
	#md = markdown.Markdown(extensions = ['toc(title=Table of Contents)', 'codehilite','meta'], output_format="html5")
	md = markdown.Markdown(extensions = ['meta']) #, output_format="xhtml5")
	#md = markdown.convert(input=mdfilefullpath,extensions = ['meta'], output_format="xhtml5")

	mktxt = mkin.read().decode('utf-8', 'xmlcharrefreplace')


	gen_html = md.convert(mktxt)
	#md.convertFile(input=mdfilefullpath,output=outfileContent, encoding="utf-8")
	#outfileContent.close()
	#infileContent = codecs.open(mdfilefullpath[0:-2] + "content", encoding="utf-8")
	md_meta =  md.Meta
	doc_title = md_meta.get('title')[0] # [0] -> converts one element list to string
	head = """<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>%(title)s</title>
  </head>
<body>
	""" % { 'title' : doc_title }
	outfile.write(head.encode('ascii', 'xmlcharrefreplace'))

	outfile.write(gen_html.encode('ascii', 'xmlcharrefreplace'))
	#outfile.write(infileContent.read())
	#infileContent.close()
	#gen_html = 

	#output.append( 
	outfile.write("""
</body>
</html>
	""".encode('ascii', 'xmlcharrefreplace'))

	
	#outfile.write(''.join(output))
	outfile.close()
	print mdfilefullpath[0:-2] + "html"

result = [os.path.join(dp, f) for dp, dn, filenames in os.walk(os.getcwd()) for f in filenames if os.path.splitext(f)[1] == '.md']

for r in result:
	convert (r)