title: Managence.com Services Conseil en organisation

<ul class="breadcrumb">
  <li><a href="/fr/">Home</a></li>
  <li><a href="/fr/services">Services</a></li>
  <li class="active">Conseil en organisation</li>
</ul>

Organisation d'équipe(s)
========================

Les équipes interconnectées, internes et externes, sont renforcées par
la maitrise commune des livrables respectives. Managence de Christopher
MANN apporte de la valeur d'abord par la compréhension des enjeux, puis
le focus matriciel des inputs, de rendus souhaités, et des besoins des
parties prises.

*Un exemple de cette collaboration réussie est dans la DSI de Wonderbox.
Christopher Mann a agit comme adjoint au DSI lors de la transition de
l'achat par Pinot-Printemps-Redoute.\
*

*"Environnement international. Il y avait une équipe de 30 développeurs,
Christopher était le lien entre les équipes fonctionnel et techniques.
Très très bon sur la partie AGILE."*

- Fatih Gezen,\

