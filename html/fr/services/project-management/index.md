title: Managence.com Gestion externalis&eacute;e de projet

<ul class="breadcrumb">
  <li><a href="/fr/">Home</a></li>
  <li><a href="/fr/services">Services</a></li>
  <li class="active">Gestion externalis&eacute;e de projet</li>
</ul>

Gestion de projet opérationelle
===============================

La gestion opérationnelle de projet fait ressortir de la valeur des
métiers de l'entreprise à travers la satisfaction cliente accrue.
Auriez-vous un projet que vous aimerez faire matérialiser ? La gestion
de projet Managence par Christopher MANN marche.

Une application de celle-ci est la maîtrise du cycle de développement
complet de dispositifs de gestion de délibérations politiques locales
depuis une démarche de l'analyse de la valeur, au développement XP et
Agile. Trois projets de logiciels libres en sont issus, dont un utilisé
dans le monde entier.

*"Christopher was a great advocate and produced world class results in
his position. I continue to be impressed with the innovation and
execution of his efforts and recommend him highly."*

*Roy Rubin,\
 Founder of Magento e-Commerce*
