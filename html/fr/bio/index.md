title: Managence.com Equipe

<ul class="breadcrumb">
  <li><a href="/fr/">Home</a></li>
  <li class="active">Equipe</li>
</ul>

Bio
---

Christopher Mann,

formé en économie et en informatique depuis

Oberlin College, Sciences Po., Supélec,

amène une expérience auprès de

Startups: Magento, MBA Multimédia, Wonderbox, ...\
 Creations: E-Delib, XSpyDer, PyrAxe, ...\
 Opensource: Frameworks, Communautés, ...\
 Business Consulting: Zend, Pro Info, ...\
 ITIL: Bureaux Veritas, Alstom, ...\
 PMO: ANPE DSI, BNP Paribas Fortis, ...
