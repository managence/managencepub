title: Managence par Christopher Mann

<ul class="breadcrumb">
  <li><a href="/fr/" class="active">Home</a></li>
</ul>

Services en projets logiciels
=============================

Managence (tm) de Christopher MANN peut vous fournir de la valeur en :

-   Spécification
-   Dévelopement
-   PMO
-   Méthode
-   Gouvernance IT
-   Conformité juridique et technique
-   Gestion
-   Conseil
-   Création logicielle

par l'application de vision focalisée sur la collaboration depuis une
expérience en projets réussis.

Nos Services ...
----------------

-   [... en pmo / planification en charges](/fr/services/pmo/)
-   [... en conseil Agile et généralement pour équipes
    collaborantes](/fr/services/team-consulting/)
-   [... en gestion de projet
    externalisée](/fr/services/project-management/)

Nous ...
--------

-   [... Bio de Christopher Mann](/fr/bio/)

