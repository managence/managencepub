title: Managence.com Services Team Consulting

<ul class="breadcrumb">
  <li><a href="/en/">Home</a></li>
  <li><a href="/en/services">Services</a></li>
  <li class="active">Team Consulting</li>
</ul>

Team Organisation
=================

Collaborative teams, internal and external, are reinforced by mutual
mastery of respective deliverables. Managence by Christopher Mann brings
value by first understanding and then facilitating the matrices of
inputs, outputs, and needs of different actors.

An example of this work is in the DSI of Wonderbox, serving as acting
assistant CIO during the transition of a purchase by
Pinot-Printemps-Redoute.

*"Environnement international. Il y avait une équipe de 30 développeurs,
Christopher était le lien entre les équipes fonctionnel et techniques.
Très très bon sur la partie AGILE."*

*- Fatih Gezen, Directeur chez ANIL IS*

