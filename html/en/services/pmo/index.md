title: Managence.com Services PMO
theme: html/en/theme.theme

<ul class="breadcrumb">
  <li><a href="/en/">Home</a></li>
  <li><a href="/en/services">Services</a></li>
  <li class="active">PMO</li>
</ul>

Process & Method
==================

Routines and objective measures create channels for reliable information
flow. Managence (tm) by Christopher Mann reinforces rigorous analysis,
common sense, and method application in multiple aspects of everyday
work.

An example of this process management expertise can be seen from work
with the French National Employment Agency. Actively maintained project
work breakdown structures with management team capacity allocation
brought both management control and co-variant portfolio influences in a
deterministic capacity-planning solution.

*I hired Christopher as a PMO for several missions. His kindness
equalshis professionalism and his creativity. I do recommend him in any
situationwhere thinking out of the box is required!*

*- Fadi Gemayel, CEO of Daylight Group*\

