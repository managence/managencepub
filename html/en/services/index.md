title: Managence.com Services

<ul class="breadcrumb">
  <li><a href="/en/">Home</a></li>
  <li class="active">Services</li>
</ul>

Services
========

Managence (tm) by Christopher Mann brings IT project and organizational
services calibrated to the needs of the client and end-users.

Key strengths in rigorous conceptualization and state-of-the-art
approaches allow per-diem consulting to bring you long-lasting value.

[Team Organisation](/en/services/team-consulting/)
--------------------------------------------------

Collaborative teams, internal and external, are reinforced by mutual
mastery of respective deliverables. Managence by Christopher Mann brings
value by first understanding and then facilitating the matrices of
inputs, outputs, and needs of different actors... [read on
...](/en/services/team-consulting/)

[Processus & Method](/en/services/pmo/)
---------------------------------------

Routines and objective measures create channels for reliable information
flow. Managence (tm) by Christopher Mann reinforces rigorous analysis,
common sense, and method application in multiple aspects of everyday
work... [read on ...](/en/services/pmo/)

[Operational Project Management](/en/services/project-management/)
------------------------------------------------------------------

Operational project management materializes business needs with
increased added value and customer satisfaction. Do you have a project,
IT or otherwise, that you would like to get off the ground? Managence by
Christopher Mann project management works... [read on
...](/en/services/project-management/)

