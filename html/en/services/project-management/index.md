title: Managence.com Services Project Management

<ul class="breadcrumb">
  <li><a href="/en/">Home</a></li>
  <li><a href="/en/services">Services</a></li>
  <li class="active">Project Management</li>
</ul>

Operational Project Management
==============================

Operational project management materializes business needs with
increased added value and customer satisfaction. Do you have a project,
IT or otherwise, that you would like to get off the ground? Managence by
Christopher Mann project management works.

An example of this work is in the mastery of a full operational project
cycle in software for town ordinances : from value-added analysis to
Agile and extreme programming to rollout. The result was three distinct
open-source projects, one of which is in active use throughout the
world.

*"Christopher was a great advocate and produced world class results in
his position. I continue to be impressed with the innovation and
execution of his efforts and recommend him highly."*

*Roy Rubin,\
 Founder of Magento e-Commerce*

