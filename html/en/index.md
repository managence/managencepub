title: Managence par Christopher Mann

<ul class="breadcrumb">
  <li><a href="/fr/" class="active">Home</a></li>
</ul>

Software Project Services
=========================

Managence (tm) by Christopher Mann can bring you value in:\

-   Specification
-   Development
-   Program and Project Management Office
-   Methods and Methodologies
-   IT Gouvernance
-   Legal and Compliance
-   Management
-   Business Consulting
-   Original Software Creation

by innovating for businesses drawing on extensive experience in internet
projects applying invention with vision and great respect for
collaboration.

Services ...
------------

-   [... in PMO and Capacity Planning](/en/services/pmo)
-   [... in Agile Consulting](/en/services/team-consulting)
-   [... in Externalized Project
    Management](/en/services/project-management)

Who we are ...
--------------

-   [... Christopher Mann](/en/bio/)

